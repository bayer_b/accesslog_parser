#ifndef PARSER_HPP_INCLUDED
#define PARSER_HPP_INCLUDED

//#define BOOST_SPIRIT_DEBUG
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/lexical_cast.hpp>

#include <boost/foreach.hpp>

//#include <typeinfo>
#include <string>
#include <map>


namespace parser
{

	namespace qi = boost::spirit::qi;
	namespace phx = boost::phoenix;

	struct ParseError
	{
		std::string message;
		ParseError(std::string& arg) : message(arg) { };
	};


	template <typename Iterator>
		struct Grammar : qi::grammar<Iterator, std::string(), qi::space_type>
	{


		Grammar(std::vector<std::string>& _logformat) : Grammar::base_type(joint_rule), logformat(_logformat)
		{
			using namespace qi;

			ip_rule 	%= lexeme[ (+char_("0-9."))[phx::ref(ip)=_1]];
			timestamp_rule	%= lexeme[ ('[' >> +(~char_(']')) >> ']')[phx::ref(timestamp)=_1]];
			user_rule	%= lexeme[ (+~char_(" "))];
			req_rule	%= lexeme[ ('"' >> +(~char_('"')) >> '"')[phx::ref(req)=_1]];
			ref_rule	%= lexeme[ ('"' >> +(~char_('"')) >> '"')[phx::ref(referer)=_1]];
			ua_rule		%= lexeme[ ('"' >> +(~char_('"')) >> '"')[phx::ref(ua)=_1]];
			bytes_rule	%= lexeme[ (+~char_(" "))[phx::ref(transferred_bytes)=_1]];
			status_rule	%= uint_[phx::ref(status)=_1];
			any_par_rule	%= lexeme[('"' >> +(~char_('"')) >> '"')];

			auto convert_logformat = std::map<std::string, rule_t> {
				{ "%h", ip_rule},
					{ "%t", timestamp_rule},
					{ "%r", req_rule},
					{ "%>s", status_rule},
					{ "%b", bytes_rule},
					{ "%u", user_rule},
					{ "%{User-agent}i", ua_rule},
					{ "%{Referer}i", ref_rule},
					{".*", any_par_rule}
			};

			joint_rule = eps;
			for (auto const& p: logformat)
			{
				joint_rule = joint_rule.copy() >> convert_logformat[p].copy();
			}
		}


		typedef qi::rule<Iterator, std::string(), qi::space_type> rule_t;
		rule_t ip_rule, timestamp_rule, user_rule, req_rule, ref_rule, ua_rule, bytes_rule, status_rule, any_par_rule;
		rule_t joint_rule;

		std::vector<std::string> logformat;
		std::vector<char> ip;
		std::vector<char> timestamp;
		std::vector<char> req;
		unsigned int status;
		std::vector<char> transferred_bytes;
		std::vector<char> referer;
		std::vector<char> ua;
	};

	template <typename Iterator>
		void parse_logentry(Iterator first, Iterator last, Grammar<Iterator>& parser)
		{
			if(!phrase_parse(first, last, parser, qi::space))
			{
				std::string message("could not parse the entry ");
				std::string entry(first, last);
				message += entry;
				throw ParseError(message);
			}

		};

}

#endif
