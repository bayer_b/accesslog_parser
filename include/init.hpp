#ifndef INIT_HPP_INCLUDED
#define INIT_HPP_INCLUDED

#include <fstream>
#include <boost/tokenizer.hpp>
#include <vector>
#include <string>


namespace init
{

void init(char**, std::ifstream&, std::vector<std::string>&, std::vector<int>&);

}

#endif

