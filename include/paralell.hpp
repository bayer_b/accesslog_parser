#ifndef PARALELL_HPP_INCLUDED
#define PARALELL_HPP_INCLUDED

#include <fstream>
#include <queue>
#include <string>

#include "parser.hpp"
#include "statistics.hpp"
#include <thread>
#include <chrono>
#include <atomic>

namespace paralell
{

	struct parser_result
	{
		std::vector<char> ip;
		std::vector<char> timestamp;
		std::vector<char> transferred_bytes;

		parser_result() {};
		~parser_result() {};
		parser_result(const parser::Grammar<std::string::iterator>&);
	};

	typedef std::queue<parser_result> parser_results;

	void read_lines(std::ifstream&, std::queue<std::string>&);

	void parse_line_and_update_statistics(std::queue<std::string>&,
		parser::Grammar<std::string::iterator>&, statistics::Statistics&);

	void parse_line(std::queue<std::string>&, parser_results&,
		std::vector<std::string>&);

	void update_statistics(parser_results&, statistics::Statistics&,
		std::vector<std::string>&);


}


#endif

