#ifndef STATISTICS_HPP_INCLUDED
#define STATISTICS_HPP_INCLUDED

#include <string>
#include <sstream>
#include <locale>
#include <ctime>
#include <cmath>
#include <boost/date_time/local_time/local_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

#include <unordered_set>

#include <iostream>


namespace statistics
{
	using boost::local_time::local_date_time;
	using boost::local_time::local_time_input_facet;
	using boost::gregorian::date_facet;

	typedef struct Entry
	{
		unsigned int numberof_visitors;
		unsigned int numberof_req[5];
		float numberof_req_av;
		unsigned int numberof_req_min;
		unsigned int numberof_req_max;
		float transferred_bytes[5];
		float transferred_bytes_sum;

		std::unordered_set<std::string> ips;
		Entry(): numberof_visitors(0), numberof_req_av(0), numberof_req_min(0), numberof_req_max(0), transferred_bytes_sum(0)
		{

			for (int i = 0; i < 5; i++)
			{
				numberof_req[i] = 0;
				transferred_bytes[i] = 0;
			}
		}

	} EntryContainerType;


	class Statistics
	{
		public:
			Statistics(const char*, std::vector<int>&);
			~Statistics();

			void add_entry (std::vector<char>&, std::vector<char>&, std::vector<char>&);
			void normalize_bins();

			friend std::ostream& operator<<(std::ostream&, const Statistics&);

		private:
			const char* timestamp_format;
			std::stringstream timestamp_parser_helper;
			std::string dummy_string;
			local_date_time timestamp_dummy;
			std::tm ctime_dummy;
			EntryContainerType *entries;
			unsigned int dummy_transferred_bytes;
			local_time_input_facet *input_facet;
			std::vector<int> on_day;
	};


}
#endif
