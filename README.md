This program is for creating the following statistics from an access log for a specific day: every row contains measures for a 5 minutes interval in the following order:

month, day, hour, minute, transferred bytes, number of visitors, the min of the number of requests in a minute, the average of the number of requests in a minute, the max of the number of requests in a minute.

Usage:

bin/access_parser <path_to_logfile> <log format> <timestamp format> <processing day in YYY-MM-DD format>
