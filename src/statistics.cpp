#include "statistics.hpp"

using boost::local_time::not_a_date_time;
using boost::gregorian::date_facet;
using boost::local_time::local_time_facet;
using boost::local_time::to_tm;

statistics::Statistics::Statistics(const char* _timestamp_format, std::vector<int>& _on_day): timestamp_format(_timestamp_format), timestamp_dummy(not_a_date_time), on_day(_on_day)
{
	input_facet = new local_time_input_facet(timestamp_format);
	timestamp_parser_helper.imbue(std::locale(std::locale::classic(), input_facet));
	timestamp_parser_helper.exceptions(std::ios::failbit);
	entries = new EntryContainerType [288];
}

statistics::Statistics::~Statistics()
{
	delete [] entries;
}

void statistics::Statistics::add_entry(std::vector<char>& ip_vecchar, std::vector<char>& timestamp, std::vector<char>& transferred_bytes_vecchar)
{
	dummy_string = std::string(timestamp.begin(), timestamp.end());
	timestamp_parser_helper.str("");
	timestamp_parser_helper << dummy_string;
	timestamp_parser_helper >> timestamp_dummy;
	ctime_dummy = to_tm(timestamp_dummy);

	if ((on_day[0] == (ctime_dummy.tm_year+1900)) && (on_day[1] == (ctime_dummy.tm_mon+1)) && (on_day[2] == (ctime_dummy.tm_mday)) )
	{
		int minutes = (ctime_dummy.tm_hour*60+ctime_dummy.tm_min);
		entries[minutes/5].numberof_req[minutes%5]++;

		dummy_string = std::string(ip_vecchar.begin(), ip_vecchar.end());
		entries[minutes/5].ips.insert(dummy_string);

		dummy_string = std::string(transferred_bytes_vecchar.begin(), transferred_bytes_vecchar.end());
		if ((dummy_string.compare("-") != 0) && (dummy_string.compare("0") != 0))
		{
			dummy_transferred_bytes = std::stoi(dummy_string);
			entries[minutes/5].transferred_bytes[minutes%5] = entries[minutes/5].transferred_bytes[minutes%5]
										+ dummy_transferred_bytes;
			entries[minutes/5].transferred_bytes_sum = entries[minutes/5].transferred_bytes_sum + dummy_transferred_bytes;
		}
	}
}

void statistics::Statistics::normalize_bins()
{
	for (int i = 0; i < 288; i++)
	{
		entries[i].numberof_req_max = entries[i].numberof_req[0];
		entries[i].numberof_req_min = entries[i].numberof_req[0];
		for (int j = 0; j < 5; j++)
		{
			entries[i].numberof_req_av = entries[i].numberof_req_av + entries[i].numberof_req[j];
			if (entries[i].numberof_req[j] > entries[i].numberof_req_max)
				entries[i].numberof_req_max = entries[i].numberof_req[j];
			if (entries[i].numberof_req[j] < entries[i].numberof_req_min)
				entries[i].numberof_req_min = entries[i].numberof_req[j];

		}

		if (entries[i].numberof_req_av != 0)
			entries[i].numberof_req_av = entries[i].numberof_req_av / 5;
		entries[i].numberof_visitors = entries[i].ips.size();
	}

}


namespace statistics
{

	std::ostream& operator<<(std::ostream& out, const Statistics& St)
	{

		int minutes;
		int year = St.on_day[0];
		int month = St.on_day[1];
		int day = St.on_day[2];
		for (int i = 0; i < 288; i++)
		{
			minutes = i*5+5;
			out << year << std::setfill('0')
				<< std::setw(2) << month 
				<< std::setw(2) << day << " "
				<< std::setw(2) << minutes/60%24 
				<< std::setw(2) << minutes%60 << "\t" 
				<< St.entries[i].transferred_bytes_sum << "\t"
				<< St.entries[i].numberof_visitors << "\t"
				<< St.entries[i].numberof_req_min << "\t"
				<< St.entries[i].numberof_req_av << "\t"
				<< St.entries[i].numberof_req_max << "\n";
		}

		return out;
	}
}
