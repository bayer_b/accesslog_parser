#include <iostream>
#include <fstream>
#include <boost/tokenizer.hpp>
//#include "parser2.hpp"
#include "parser.hpp"
#include "statistics.hpp"
#include "init.hpp"
#include "paralell.hpp"
#include <vector>
#include <string>

#include <thread>
#include <functional>
#include <future>


int main( int argc, char **argv )
{

	std::ifstream logfile;
	std::vector<std::string> logformat;
	std::vector<int> on_day;
	init::init(argv, logfile, logformat, on_day);

	paralell::parser_results Parsers;
	std::string entry;
	std::queue<std::string> entries;

	statistics::Statistics Stats(argv[3], on_day);


	try
	{
		std::thread t_read(paralell::read_lines, std::ref(logfile), std::ref(entries));
		
		std::thread t_parse(paralell::parse_line, std::ref(entries), std::ref(Parsers), std::ref(logformat));
		std::thread t_update(paralell::update_statistics, std::ref(Parsers), std::ref(Stats),
			std::ref(logformat));

		t_read.join();

		t_parse.join();

		t_update.join();

		logfile.close();

		Stats.normalize_bins();

	}
	catch ( parser::ParseError& e )
	{
		std::cout << "logentry parse error occured: " << e.message << "\n";
		return 1;
	}
	catch ( std::invalid_argument& e )
	{
		std::cout << "no conversion could be performed: " << e.what() << "\n";
		return 1;
	}
	catch ( std::exception& e )
	{
		std::cout << "Unhandled exception occured at \"" << entry << "\": " << e.what() << "\n";
		return 1;
	}

	return 0;
}

