#include "paralell.hpp"

static std::mutex queue_mutex;
static std::mutex parser_mutex;
static std::atomic<bool> finished_reading(false);
static std::atomic<bool> finished_parsing(false);

paralell::parser_result::parser_result(const parser::Grammar<std::string::iterator>& Parser)
{
	ip = Parser.ip;
	timestamp = Parser.timestamp;
	transferred_bytes = Parser.transferred_bytes;
};


void paralell::read_lines(std::ifstream &file, std::queue<std::string>& entries)
{
auto start = std::chrono::monotonic_clock::now();
std::cout << "read_lines thread started" << "\n";
	std::unique_lock<std::mutex> lk(queue_mutex);
	lk.unlock();
	std::string entry;
	while (std::getline(file, entry))
	{
		lk.lock();
		entries.push(entry);
		lk.unlock();
	}
auto end = std::chrono::monotonic_clock::now();
auto duration = end - start;
std::cout << "read_lines thread end reached. It took "
	<< std::chrono::duration <double, std::milli> (duration).count() << " ms." << std::endl << "\n";
	finished_reading = true;
}

void paralell::parse_line_and_update_statistics(std::queue<std::string>& entries,
		parser::Grammar<std::string::iterator>& Parser, statistics::Statistics& Stats)
{
auto start = std::chrono::monotonic_clock::now();
std::cout << "parse_lines_and_update_statistics thread started" << "\n";
	std::unique_lock<std::mutex> lk(queue_mutex);
	lk.unlock();
	std::string line;
	while (!finished_reading)
	{
		if (!entries.empty())
		{	
			lk.lock();
			line = entries.front();
			entries.pop();
			lk.unlock();
			parser::parse_logentry(line.begin(), line.end(), Parser);
			
			Stats.add_entry(Parser.ip, Parser.timestamp, Parser.transferred_bytes);
			
		}
	}
auto end = std::chrono::monotonic_clock::now();
auto duration = end - start;
std::cout << "parse_lines_and_update_statistics thread end reached. It took "
	<< std::chrono::duration <double, std::milli> (duration).count() << " ms." << std::endl << "\n";
}

void paralell::parse_line(std::queue<std::string>& entries,
	paralell::parser_results& Parser_results, std::vector<std::string>& logformat)
{
auto start = std::chrono::monotonic_clock::now();
std::cout << "parse_lines thread started" << "\n";
	std::unique_lock<std::mutex> queue_lk(queue_mutex);
	queue_lk.unlock();
	std::unique_lock<std::mutex> parser_lk(parser_mutex);
	parser_lk.unlock();
	std::string line;
	parser::Grammar<std::string::iterator> Parser(logformat);
	
	while (!finished_reading)
	{
		if (!entries.empty())
		{	
			queue_lk.lock();
			line = entries.front();
			entries.pop();
			queue_lk.unlock();
			parser::parse_logentry(line.begin(), line.end(), Parser);
			parser_lk.lock();
			paralell::parser_result Parser_result(Parser);
			Parser_results.push(Parser_result);
			parser_lk.unlock();
		}
	}
auto end = std::chrono::monotonic_clock::now();
auto duration = end - start;
std::cout << "parse_lines thread end reached. It took "
	<< std::chrono::duration <double, std::milli> (duration).count() << " ms." << std::endl << "\n";
	finished_parsing = true;
}

void paralell::update_statistics(paralell::parser_results& Parser_results,
	statistics::Statistics& Stats, std::vector<std::string>& logformat)
{	
auto start = std::chrono::monotonic_clock::now();
std::cout << "update_statistics thread started" << "\n";

	std::unique_lock<std::mutex> parser_lk(parser_mutex);
	parser_lk.unlock();
	paralell::parser_result Parser_result;
	while (!finished_parsing)
	{
		if (!Parser_results.empty())
		{
			parser_lk.lock();
			Parser_result = Parser_results.front();
			Parser_results.pop();
			parser_lk.unlock();
			Stats.add_entry(Parser_result.ip, Parser_result.timestamp, Parser_result.transferred_bytes);
		}
	}

auto end = std::chrono::monotonic_clock::now();
auto duration = end - start;
std::cout << "update_statistics thread end reached. It took "
	<< std::chrono::duration <double, std::milli> (duration).count() << " ms." << std::endl << "\n";
}