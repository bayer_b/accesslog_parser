#include "init.hpp"


void init::init(char** argv, std::ifstream& logfile, std::vector<std::string>& logformat, std::vector<int>& on_day)
{
	logfile.open(argv[1], std::ifstream::in);
	boost::char_separator<char> sep(" ");
	std::string helper(argv[2]);
	boost::tokenizer<boost::char_separator<char> > tokens(helper, sep);

	for (const auto& i : tokens )
	{
		logformat.push_back(i);
	}

	helper = std::string(argv[4]);
	sep = boost::char_separator<char>("-");
	tokens = boost::tokenizer<boost::char_separator<char> >(helper, sep);

	for (const auto& i : tokens )
	{
		on_day.push_back(std::stoi(i));
	}

}
