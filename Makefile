CC              = g++-4.6
CFLAGS          = -O3 -std=c++0x -ggdb -Wall -pthread
#CFLAGS          = -O3 -std=c++0x -Wall -static
SOURCES         = src/main.cpp src/statistics.cpp src/init.cpp src/paralell.cpp
OBJECT_DIR      = objs
INCLUDES        = -I./include/
OUTPUT          = bin/access_parser
_OBJECTS        = main.o statistics.o init.o paralell.o

OBJECTS = $(patsubst %,$(OBJECT_DIR)/%,$(_OBJECTS))
SOURCES = $(patsubst %,%.cpp,$(_SOURCES))
$(OUTPUT): $(OBJECTS)
	$(CC) -o $@ $^ $(INCLUDES) $(CFLAGS)

$(OBJECT_DIR)/main.o: src/main.cpp
	$(CC) -c $^ -o $@ $(INCLUDES) $(CFLAGS)

$(OBJECT_DIR)/statistics.o: src/statistics.cpp
	$(CC) -c $^ -o $@ $(INCLUDES) $(CFLAGS)

$(OBJECT_DIR)/init.o: src/init.cpp
	$(CC) -c $^ -o $@ $(INCLUDES) $(CFLAGS)

$(OBJECT_DIR)/paralell.o: src/paralell.cpp
	$(CC) -c $^ -o $@ $(INCLUDES) $(CFLAGS)
	
clean:
	rm -f $(OBJECT_DIR)/*.o $(OUTPUT)

